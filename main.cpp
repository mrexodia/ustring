#include <iostream>
#include "UString/UString.h"

int main()
{
    //initialize a UTF-16 string and print the contents
    std::wstring utf16_1 = L"\u0710 \u075D \u1D25 \u1EA3 \u2603"; //some unicode characters
    for(unsigned int i = 0; i < utf16_1.length(); i++)
    {
        printf("%.4X ", (unsigned short)utf16_1.c_str()[i]);
    }
    puts("");

    //convert to UTF-8 and print the result
    UString utf8 = ConvertUtf16ToUtf8(utf16_1);
    for(unsigned int i = 0; i < utf8.DataLength(); i++)
    {
        printf("%.2X ", (unsigned char)utf8.c_str()[i]);
    }
    puts("");

    //convert back to UTF-16 and print the result
    std::wstring utf16_2 = ConvertUtf8ToUtf16(utf8);
    for(unsigned int i = 0; i < utf16_2.length(); i++)
    {
        printf("%.4X ", (unsigned short)utf16_2.c_str()[i]);
    }
    puts("");

    //check if our functions worked
    if(utf16_1 == utf16_2)
        puts("all ok!");
    else
        puts("problems with UTF8 conversion");
    return 0;
}
